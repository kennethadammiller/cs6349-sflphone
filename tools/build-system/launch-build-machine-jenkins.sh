#!/bin/bash
#####################################################
# File Name: launch-build-machine-jenkins.sh
#
# Purpose :
#
# Author: Julien Bonjean (julien@bonjean.info)
#
# Creation Date: 2009-10-20
# Last Modified: 2010-04-22 16:42:57 -0400
#####################################################

set -x

. `dirname $0`/setenv.sh

IS_RELEASE=
VERSION_INDEX="1"
IS_KDE_CLIENT=
DO_PUSH=1
DO_LOGGING=1
DO_UPLOAD=1
SNAPSHOT_TAG=`date +%Y%m%d`
TAG_NAME_PREFIX=
VERSION_NUMBER="1.3.0"

LAUNCHPAD_PACKAGES=("sflphone-common" "sflphone-client-kde" "sflphone-client-gnome" "sflphone-plugins" "sflphone-common-video" "sflphone-client-gnome-video")

echo
echo "    /***********************\\"
echo "    | SFLPhone build system |"
echo "    \\***********************/"
echo

for PARAMETER in $*
do
        case ${PARAMETER} in
        --help)
                echo
                echo "Options :"
                echo " --skip-push"
                echo " --skip-upload"
                echo " --kde-client"
                echo " --no-logging"
                echo " --release"
                echo " --version-index=[1,2,...]"
                echo
                exit 0;;
        --skip-push)
                unset DO_PUSH;;
        --skip-upload)
                unset DO_UPLOAD;;
        --kde-client)
                IS_KDE_CLIENT=1;;
        --no-logging)
                unset DO_LOGGING;;
        --release)
                IS_RELEASE=1;;
        --version-index=*)
                VERSION_INDEX=(${PARAMETER##*=});;
        *)
                echo "Unknown parameter : ${PARAMETER}"
                exit -1;;
        esac
done

#########################
# LAUNCHPAD
#########################

# change to working directory
cd ${LAUNCHPAD_DIR}

if [ "$?" -ne "0" ]; then
        echo " !! Cannot cd to launchpad directory"
        exit -1
fi

# logging
if [ ${DO_LOGGING} ]; then

	rm -f ${ROOT_DIR}/packaging.log >/dev/null 2>&1

	# open file descriptor
	exec 3<> ${ROOT_DIR}/packaging.log

	# redirect outputs (stdout & stderr)
	exec 1>&3
	exec 2>&3
fi

if [ ${RELEASE_MODE} ]; then
	echo "Release mode"
else
	echo "Snapshot mode"
fi

if [ ${IS_KDE_CLIENT} ]; then
	TAG_NAME_PREFIX="kde."
fi

#########################
# COMMON PART
#########################

cd ${REFERENCE_REPOSITORY}

echo "Update reference sources"
if [ ${IS_RELEASE} ]; then
        git checkout . && git checkout -f release && git pull
else
        git checkout . && git checkout -f master && git pull
fi

echo "Retrieve build info"
# retrieve info we may need
if [ ${IS_KDE_CLIENT} ]; then
	TAG_NAME_PREFIX="kde."
	LAUNCHPAD_PACKAGES=( "sflphone-client-kde" )
fi
CURRENT_RELEASE_TAG_NAME=`git describe --tags --abbrev=0`
PREVIOUS_RELEASE_TAG_NAME=`git describe --tags --abbrev=0 ${CURRENT_RELEASE_TAG_NAME}^`
CURRENT_RELEASE_COMMIT_HASH=`git show --pretty=format:"%H" -s ${CURRENT_RELEASE_TAG_NAME} | tail -n 1`
PREVIOUS_RELEASE_COMMIT_HASH=`git show --pretty=format:"%H" -s ${PREVIOUS_RELEASE_TAG_NAME} | tail -n 1`
CURRENT_COMMIT=`git show --pretty=format:"%H"  -s | tail -n 1`
CURRENT_RELEASE_TYPE=${CURRENT_RELEASE_TAG_NAME##*.}
PREVIOUS_RELEASE_TYPE=${PREVIOUS_RELEASE_TAG_NAME##*.}
if [ ${IS_KDE_CLIENT} ]; then
	CURRENT_RELEASE_VERSION=${CURRENT_RELEASE_TAG_NAME%.*}
	CURRENT_RELEASE_VERSION=${CURRENT_RELEASE_VERSION#*.}
	PREVIOUS_VERSION=${PREVIOUS_RELEASE_TAG_NAME%.*}
	PREVIOUS_VERSION=${PREVIOUS_VERSION#*.}
else
	CURRENT_RELEASE_VERSION=${CURRENT_RELEASE_TAG_NAME}
	PREVIOUS_VERSION=${PREVIOUS_RELEASE_TAG_NAME}
fi

cd ${LAUNCHPAD_DIR}

COMMIT_HASH_BEGIN=""
COMMIT_HASH_END=""
SOFTWARE_VERSION=""
LAUNCHPAD_CONF_PREFIX=""

if [ ${IS_RELEASE} ]; then
	SOFTWARE_VERSION="${CURRENT_RELEASE_VERSION}"
	COMMIT_HASH_BEGIN="${PREVIOUS_RELEASE_COMMIT_HASH}"
	LAUNCHPAD_CONF_PREFIX="sflphone"
else
	SOFTWARE_VERSION="${VERSION_NUMBER}-rc${SNAPSHOT_TAG}"
	COMMIT_HASH_BEGIN="${CURRENT_RELEASE_COMMIT_HASH}"
	LAUNCHPAD_CONF_PREFIX="sflphone-nightly"
fi

VERSION="${SOFTWARE_VERSION}~ppa${VERSION_INDEX}~SYSTEM"

echo "Clean build directory"
git clean -f -x ${LAUNCHPAD_DIR}/* >/dev/null

get_dir_name() {
    case $1 in
        sflphone-common)
        echo daemon
        ;;
        sflphone-common-video)
        echo daemon
        ;;
        sflphone-plugins)
        echo plugins
        ;;
        sflphone-client-gnome)
        echo gnome
        ;;
        sflphone-client-gnome-video)
        echo gnome
        ;;
        sflphone-client-kde)
        echo kde
        ;;
        *)
        exit 1
        ;;
    esac
}

for LAUNCHPAD_PACKAGE in ${LAUNCHPAD_PACKAGES[*]}
do
	echo " Package: ${LAUNCHPAD_PACKAGE}"

	echo "  --> Clean old sources"
	git clean -f -x ${LAUNCHPAD_DIR}/${LAUNCHPAD_PACKAGE}/* >/dev/null

	DEBIAN_DIR="${LAUNCHPAD_DIR}/${LAUNCHPAD_PACKAGE}/debian"

	echo "  --> Clean debian directory"
	git checkout ${DEBIAN_DIR}

	echo "  --> Retrieve new sources"
	DIRNAME=`get_dir_name ${LAUNCHPAD_PACKAGE}`
	cp -r ${REFERENCE_REPOSITORY}/${DIRNAME}/* ${LAUNCHPAD_DIR}/${LAUNCHPAD_PACKAGE}

	echo "  --> Update software version number (${SOFTWARE_VERSION})"
	echo "${SOFTWARE_VERSION}" > ${LAUNCHPAD_DIR}/${LAUNCHPAD_PACKAGE}/VERSION

	echo "  --> Update debian changelog"

cat << END > ${WORKING_DIR}/sfl-git-dch.conf
WORKING_DIR="${REFERENCE_REPOSITORY}"
SOFTWARE="${LAUNCHPAD_PACKAGE}"
VERSION="${VERSION}"
DISTRIBUTION="SYSTEM"
CHANGELOG_FILE="${DEBIAN_DIR}/changelog"
COMMIT_HASH_BEGIN="${COMMIT_HASH_BEGIN}"
COMMIT_HASH_END="${COMMIT_HASH_END}"
IS_RELEASE=${IS_RELEASE}
export DEBFULLNAME="Emmanuel Milou"
export DEBEMAIL="emmanuel.milou@savoirfairelinux.com"
export EDITOR="echo"
END

	${WORKING_DIR}/sfl-git-dch-2.sh ${WORKING_DIR}/sfl-git-dch.conf ${REFERENCE_REPOSITORY}/${DIRNAME}/
	if [ "$?" -ne "0" ]; then
		echo "!! Cannot update debian changelogs"
		exit -1
	fi

	if [ "${LAUNCHPAD_PACKAGE}"  == "sflphone-client-kde" ]; then
		version_kde=$(echo ${VERSION}  | grep -e '[0-9]*\.[0-9.]*' -o | head -n1)
		sed -i -e "s/Standards-Version: [0-9.A-Za-z]*/Standards-Version: ${version_kde}/" ${LAUNCHPAD_DIR}/${LAUNCHPAD_PACKAGE}/debian/control
		tar -C ${LAUNCHPAD_DIR}/ -cjf ${LAUNCHPAD_DIR}/sflphone-client-kde_${version_kde}.orig.tar.bz2  ${LAUNCHPAD_PACKAGE}
	fi

	rm -f ${WORKING_DIR}/sfl-git-dch.conf >/dev/null 2>&1

	cd ${LAUNCHPAD_DIR}

	cp ${DEBIAN_DIR}/changelog ${DEBIAN_DIR}/changelog.generic

	for LAUNCHPAD_DISTRIBUTION in ${LAUNCHPAD_DISTRIBUTIONS[*]}
	do

		LOCAL_VERSION="${SOFTWARE_VERSION}~ppa${VERSION_INDEX}~${LAUNCHPAD_DISTRIBUTION}"

		cp ${DEBIAN_DIR}/changelog.generic ${DEBIAN_DIR}/changelog

		sed -i "s/SYSTEM/${LAUNCHPAD_DISTRIBUTION}/g" ${DEBIAN_DIR}/changelog

		cd ${LAUNCHPAD_DIR}/${LAUNCHPAD_PACKAGE}
		if [ "${LAUNCHPAD_PACKAGE}"  != "sflphone-client-kde" ]; then
			./autogen.sh
		fi
		debuild -S -sa -kF5362695
		cd ${LAUNCHPAD_DIR}

		if [ ${DO_UPLOAD} ] ; then
			dput -f --debug --no-upload-log -c ${LAUNCHPAD_DIR}/dput.conf ${LAUNCHPAD_CONF_PREFIX}-${LAUNCHPAD_DISTRIBUTION} ${LAUNCHPAD_PACKAGE}_${LOCAL_VERSION}_source.changes
		fi
	done

	cp ${DEBIAN_DIR}/changelog.generic ${DEBIAN_DIR}/changelog
done

# if push is activated
#if [[ ${DO_PUSH} && ${IS_RELEASE} ]];then
#	echo " Doing commit"
#	git commit -m "[#1262] Released ${SOFTWARE_VERSION}" .
#
#	echo " Pushing commit"
#	git push origin release
#fi


# Archive source tarball for Debian maintainer
. `dirname $0`/build_tarball.sh ${SOFTWARE_VERSION}


# close file descriptor
exec 3>&-

exit 0
