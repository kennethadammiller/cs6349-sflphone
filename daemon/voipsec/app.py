# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import sys
dir = "/usr/local/share/sflphone/voipsec/"
sys.path.append(dir )
from twisted.internet.protocol import DatagramProtocol, Factory
from twisted.internet import reactor, ssl
#import argparse
import logging

from udp_proxy import UdpProxy
from ssl_receiver import SslReceiverFactory
from rtp_listener import RtpListener

def parse_args():
    parser = argparse.ArgumentParser(description='A secure SIP proxy')
    parser.add_argument('--sip-port', type=int, help='The UDP port to listen on',default=15060)
    parser.add_argument('--ssl-port', type=int, help='The SSL port to listen on for encrypted SSL', default=10433)
    parser.add_argument('--debug', action='store_true', help='Enable debug log messages')
    parser.add_argument('--rtp-port', type=int, help='The UDP port to listen on for RTP.', default=62568)

    args = parser.parse_args()

    return args

def config_logging(is_debug):
    formatter = logging.Formatter('%(asctime)s - %(levelname)s[%(name)s] - %(message)s', '%H:%M:%S')
    level = logging.DEBUG if is_debug else logging.INFO

    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    ch.setLevel(level)

    logger = logging.getLogger()
    logger.setLevel(level)
    logger.addHandler(ch)

#args = parse_args()
config_logging(None)

hostname_lookup = {}
current_call = {}

# Setup UDP Proxy 
logging.info('UDP proxy listening on UDP port %d', 15060)
udp = UdpProxy(hostname_lookup, current_call)
reactor.listenUDP(15060, udp)

# Setup SSL Receiver
factory = SslReceiverFactory(udp.transport, hostname_lookup, current_call)

logging.info('SSL Listener listening on TCP port %d', 10433)
reactor.listenSSL(10433, factory, ssl.DefaultOpenSSLContextFactory(dir + '/keys/voipsec_client.key', dir + '/keys/voipsec_client.crt'))

# Setup RTP listener
logging.info('RTP listener listening for incoming on UDP port %d', 62568)
rtp_in = RtpListener(True, hostname_lookup, current_call)
reactor.listenUDP(62568, rtp_in)

logging.info('RTP listener listening for outgoing on UDP port %d', 62568 + 1)
rtp_out = RtpListener(False, hostname_lookup, current_call)
reactor.listenUDP(62568 + 1, rtp_out)

# Start listening for traffic
reactor.run(installSignalHandlers=0)
