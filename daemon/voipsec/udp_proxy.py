from twisted.internet.protocol import DatagramProtocol
from header_parser import parse_headers
import socket
from ssl import SSLSocket
import logging
import os
import base64

logger = logging.getLogger('UDP')


class UdpProxy(DatagramProtocol):
    """Receives packets from the local SIP client and transmits them over
    SSL to the remote host"""

    send_to_from = ['SIP/2.0 180 Ringing', 'SIP/2.0 200 OK']

    def __init__(self, hostname_lookup, current_call, remote_ssl_port=10433):
        self.ssl_sockets = {}
        self.remote_ssl_port = remote_ssl_port
        self.hostname_lookup = hostname_lookup
        self.current_call = current_call

    def datagramReceived(self, data, (host, port)):
        """Processes SIP messages received from the local client"""
        lines = data.split('\r\n')

        headers, info, body = parse_headers(lines)
        recp = self.determine_recp(lines[0], info, headers)
        call_id = info['call_id']

        recp_info = self.hostname_lookup[
            recp] if recp in self.hostname_lookup else info['to_host']

        # Retrieve an SSL socket
        sock = self.check_ssl_sock(recp_info)

        if sock is None:
            logger.error(
                'An SSL socket could not be established to %s!', info['to_host'])
            return
        
        # Forward the packet over the socket
        sock.sendall(self.processData(lines[:1], headers, body))

        logger.info('%s <<< %s' % (recp_info, lines[0]))

    def processData(self, first_line, headers, body):
        """Transforms the packet's contents for secure communication"""
        data = []

        # Remove the Content-Length header (we'll need it for later)
        content_len = headers.popitem()

        # Retrieve the call id
        cid = headers['Call-ID'][0]

        if first_line[0].startswith('ACK') and 'X-VoipSec-Key' not in headers:
            # If the message is an ACK without a AES key, create one
            self.current_call['key'] = os.urandom(16)

            value = base64.b64encode(self.current_call['key'])
            logger.info('Injecting AES key: %s', value)
            headers['X-VoipSec-Key'] = [value]

        # Readd the Content-Length header to the end where it belongs
        headers[content_len[0]] = content_len[1]

        # Reassemble the headers back into a string and return
        for k, v in headers.items():
            for iv in v:
                data.append(k + ': ' + iv)

        return '\r\n'.join(first_line + data + [''] + body)

    def determine_recp(self, first_line, info, headers):
        """Determine the recipient based on header contents"""
        if first_line.startswith('ACK'):
            return headers['Call-ID'][0]

        if first_line.strip() in UdpProxy.send_to_from:
            return info['from']
        else:
            return info['to']

    def check_ssl_sock(self, host):
        """Returns the SSL socket used for communicating with the specified
        host. If one does not exist yet, it is created and returned
        """

        if host not in self.ssl_sockets:
            # If the host is not in our list of SSL sockets, we need to make
            # one
            host_ip = socket.gethostbyname(host)

            sock = SSLSocket(socket.socket(socket.AF_INET, socket.SOCK_STREAM))
            sock.settimeout(3)

            try:
                sock.connect((host_ip, self.remote_ssl_port))
            except socket.timeout:
                return None

            # Keep the socket for later
            self.ssl_sockets[host] = sock

        return self.ssl_sockets[host]
