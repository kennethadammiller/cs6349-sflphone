
#ifndef __g_cclosure_user_marshal_MARSHAL_H__
#define __g_cclosure_user_marshal_MARSHAL_H__

#include	<glib-object.h>

G_BEGIN_DECLS

/* VOID:INT (marshaller.list:1) */
#define g_cclosure_user_marshal_VOID__INT	g_cclosure_marshal_VOID__INT

/* VOID:INT,INT (marshaller.list:2) */
extern void g_cclosure_user_marshal_VOID__INT_INT (GClosure     *closure,
                                                   GValue       *return_value,
                                                   guint         n_param_values,
                                                   const GValue *param_values,
                                                   gpointer      invocation_hint,
                                                   gpointer      marshal_data);

/* VOID:STRING (marshaller.list:3) */
#define g_cclosure_user_marshal_VOID__STRING	g_cclosure_marshal_VOID__STRING

/* VOID:STRING,INT (marshaller.list:4) */
extern void g_cclosure_user_marshal_VOID__STRING_INT (GClosure     *closure,
                                                      GValue       *return_value,
                                                      guint         n_param_values,
                                                      const GValue *param_values,
                                                      gpointer      invocation_hint,
                                                      gpointer      marshal_data);

/* VOID:STRING,DOUBLE (marshaller.list:5) */
extern void g_cclosure_user_marshal_VOID__STRING_DOUBLE (GClosure     *closure,
                                                         GValue       *return_value,
                                                         guint         n_param_values,
                                                         const GValue *param_values,
                                                         gpointer      invocation_hint,
                                                         gpointer      marshal_data);

/* VOID:STRING,STRING (marshaller.list:6) */
extern void g_cclosure_user_marshal_VOID__STRING_STRING (GClosure     *closure,
                                                         GValue       *return_value,
                                                         guint         n_param_values,
                                                         const GValue *param_values,
                                                         gpointer      invocation_hint,
                                                         gpointer      marshal_data);

/* VOID:STRING,INT,INT (marshaller.list:7) */
extern void g_cclosure_user_marshal_VOID__STRING_INT_INT (GClosure     *closure,
                                                          GValue       *return_value,
                                                          guint         n_param_values,
                                                          const GValue *param_values,
                                                          gpointer      invocation_hint,
                                                          gpointer      marshal_data);

/* VOID:STRING,STRING,BOOL (marshaller.list:8) */
extern void g_cclosure_user_marshal_VOID__STRING_STRING_BOOLEAN (GClosure     *closure,
                                                                 GValue       *return_value,
                                                                 guint         n_param_values,
                                                                 const GValue *param_values,
                                                                 gpointer      invocation_hint,
                                                                 gpointer      marshal_data);
#define g_cclosure_user_marshal_VOID__STRING_STRING_BOOL	g_cclosure_user_marshal_VOID__STRING_STRING_BOOLEAN

/* VOID:STRING,STRING,INT (marshaller.list:9) */
extern void g_cclosure_user_marshal_VOID__STRING_STRING_INT (GClosure     *closure,
                                                             GValue       *return_value,
                                                             guint         n_param_values,
                                                             const GValue *param_values,
                                                             gpointer      invocation_hint,
                                                             gpointer      marshal_data);

/* VOID:STRING,STRING,STRING (marshaller.list:10) */
extern void g_cclosure_user_marshal_VOID__STRING_STRING_STRING (GClosure     *closure,
                                                                GValue       *return_value,
                                                                guint         n_param_values,
                                                                const GValue *param_values,
                                                                gpointer      invocation_hint,
                                                                gpointer      marshal_data);

/* VOID:STRING,STRING,INT,INT (marshaller.list:11) */
extern void g_cclosure_user_marshal_VOID__STRING_STRING_INT_INT (GClosure     *closure,
                                                                 GValue       *return_value,
                                                                 guint         n_param_values,
                                                                 const GValue *param_values,
                                                                 gpointer      invocation_hint,
                                                                 gpointer      marshal_data);

/* VOID:STRING,STRING,BOOL,STRING (marshaller.list:12) */
extern void g_cclosure_user_marshal_VOID__STRING_STRING_BOOLEAN_STRING (GClosure     *closure,
                                                                        GValue       *return_value,
                                                                        guint         n_param_values,
                                                                        const GValue *param_values,
                                                                        gpointer      invocation_hint,
                                                                        gpointer      marshal_data);
#define g_cclosure_user_marshal_VOID__STRING_STRING_BOOL_STRING	g_cclosure_user_marshal_VOID__STRING_STRING_BOOLEAN_STRING

/* VOID:STRING,BOXED (marshaller.list:13) */
extern void g_cclosure_user_marshal_VOID__STRING_BOXED (GClosure     *closure,
                                                        GValue       *return_value,
                                                        guint         n_param_values,
                                                        const GValue *param_values,
                                                        gpointer      invocation_hint,
                                                        gpointer      marshal_data);

G_END_DECLS

#endif /* __g_cclosure_user_marshal_MARSHAL_H__ */

